import { TestBed } from '@angular/core/testing';

import { PaginationService } from './pagination.service';

describe('PaginationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaginationService = TestBed.get(PaginationService);
    expect(service).toBeTruthy();
  });

  it('should create an array of 4 elements', () => {
    const service: PaginationService = TestBed.get(PaginationService);
    const numberOfElements = 32;
    const maxItemsPerPage = 10;
    const pagerArray = service.generateArrayOfPages(numberOfElements, maxItemsPerPage);
    expect(pagerArray.length).toEqual(4);
  });
});
