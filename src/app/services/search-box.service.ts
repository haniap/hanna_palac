import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {RestService} from './rest.service';
import {Character} from '../models/character.model';

@Injectable({
  providedIn: 'root'
})
export class SearchBoxService {
  searchText$ = new Subject<string>();
  queryLength = 0;

  constructor(private restService: RestService) {
  }

  fetchSearchResults(): Observable<Character[]> {
    return this.searchText$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(query => this.restService.getSearchResult(query))
    );
  }
}
