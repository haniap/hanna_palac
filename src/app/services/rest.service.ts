import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Character} from '../models/character.model';
import {catchError, debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient, @Inject('CONFIG') readonly config) {
  }

  getCharacters(page: number): Observable<HttpResponse<any>> {
    return this.http.get<any>(`${this.config.restApiServer}/characters?_page=${page}`, {observe: 'response'})
      .pipe(catchError(this.handleError));
  }

  getCharacter(id: number): Observable<Character> {
    return this.http.get<Character>(`${this.config.restApiServer}/characters/${id}`)
      .pipe(catchError(this.handleError));
  }

  getSearchResult(query: string): Observable<Character[]> {
    return this.http.get<Character[]>(`${this.config.restApiServer}/characters?q=${query}`)
      .pipe(debounceTime(200), distinctUntilChanged(), catchError(this.handleError));
  }

  getSpecies(): Observable<string[]> {
    return this.http.get<string[]>(`${this.config.restApiServer}/species`)
      .pipe(catchError(this.handleError));
  }

  postCharacter(character: Character): Observable<Character[]> {
    return this.http.post<Character[]>(`${this.config.restApiServer}/characters`, character, {headers: this.getHttpHeaders()})
      .pipe(catchError(this.handleError));
  }

  deleteCharacter(id: number): Observable<null> {
    return this.http.delete<null>(`${this.config.restApiServer}/characters/${id}`, {headers: this.getHttpHeaders()})
      .pipe(catchError(this.handleError));
  }

  patchCharacter(id: number, character: Character): Observable<Character[]> {
    return this.http.put<Character[]>(`${this.config.restApiServer}/characters/${id}`, character, {headers: this.getHttpHeaders()})
      .pipe(catchError(this.handleError));
  }

  getHttpHeaders(): HttpHeaders {
    return new HttpHeaders().set('content-type', 'application/json');
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage;
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
