import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {

  currentPage = 1;
  elementCount$ = new Subject<number>();

  constructor() {}

  generateArrayOfPages(numberOfElements: number, maxItemsPerPage: number) {
    const possiblePages = Math.ceil(numberOfElements / maxItemsPerPage);
    return new Array(possiblePages);
  }
}
