import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {PaginationService} from '../services/pagination.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Config} from '../../assets/config';

@Component({
  selector: 'sl-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnDestroy {
  pager: Array<number>;
  @Input() pages;
  @Output() requestedPage: EventEmitter<number> = new EventEmitter<number>();
  private unsubscribe$ = new Subject();

  constructor(
      private paginationService: PaginationService,
      private config: Config
      ) {}

  ngOnInit() {
    this.paginationService.elementCount$.pipe(takeUntil(this.unsubscribe$)).subscribe(el =>
      this.pager = this.paginationService.generateArrayOfPages(el, this.config.maxItemsPerPage));
  }

  getThisPage(page: number) {
    this.paginationService.currentPage = page;
    this.requestedPage.emit(this.paginationService.currentPage);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
