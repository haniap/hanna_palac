import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {ListViewComponent} from './list-view/list-view.component';
import {RestService} from './services/rest.service';
import {HttpClientModule} from '@angular/common/http';
import {PaginationComponent} from './pagination/pagination.component';
import {SearchBoxComponent} from './search-box/search-box.component';
import {NewCharacterComponent} from './new-character/new-character.component';
import {ReactiveFormsModule} from '@angular/forms';
import {PaginationService} from './services/pagination.service';
import {SearchBoxService} from './services/search-box.service';
import {Config} from '../assets/config';
import {FormDirective} from './new-character/sl-focus.directive';


@NgModule({
  declarations: [
    AppComponent,
    ListViewComponent,
    PaginationComponent,
    SearchBoxComponent,
    NewCharacterComponent,
    FormDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    RestService,
    {provide: 'CONFIG', useClass: Config},
    PaginationService,
    SearchBoxService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
