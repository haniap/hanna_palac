import {Component, OnDestroy, OnInit} from '@angular/core';
import {RestService} from '../services/rest.service';
import {Character} from '../models/character.model';
import {SearchBoxService} from '../services/search-box.service';
import {PaginationService} from '../services/pagination.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';


@Component({
  selector: 'sl-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit, OnDestroy {
  characters: Character[];
  isSearchResultEmpty = false;
  filteredResults = false;
  private unsubscribe$ = new Subject();

  constructor(
    private restService: RestService,
    private searchBoxService: SearchBoxService,
    private paginationService: PaginationService
  ) {
  }

  ngOnInit() {
    this.fetchCharacters(1);
    this.fetchSearchResults();
  }

  fetchCharacters(page: number) {
    if (!this.filteredResults) {
      this.restService.getCharacters(page).pipe(takeUntil(this.unsubscribe$)).subscribe(response => {
          this.paginationService.elementCount$.next(+response.headers.get('X-Total-Count'));
          this.characters = response.body;
        }
      );
    }
  }

  fetchSearchResults() {
    this.searchBoxService.fetchSearchResults().pipe(takeUntil(this.unsubscribe$)).subscribe(response => {
        this.characters = response;
        this.isSearchResultEmpty = (response.length === 0);
        this.filteredResults = true;
        this.paginationService.elementCount$.next(response.length);
        if (this.searchBoxService.queryLength === 0) {
          this.filteredResults = false;
          this.fetchCharacters(1);
        }
      }
    );
  }

  deleteCharacter(id: number) {
    this.restService.deleteCharacter(id).pipe(takeUntil(this.unsubscribe$)).subscribe(
      () => {
        this.characters.splice(id, 1);
        this.characters = this.characters.filter(character => character.id !== id);
        window.alert('You deleted a character with id: ' + id);
        this.fetchCharacters(this.paginationService.currentPage);
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
