import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ListViewComponent} from './list-view/list-view.component';
import {NewCharacterComponent} from './new-character/new-character.component';

const routes: Routes = [
  {
    path: '',
    component: ListViewComponent
  },
  {
    path: 'addCharacter', component: NewCharacterComponent
  },
  {
    path: 'editCharacter/:id', component: NewCharacterComponent
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
