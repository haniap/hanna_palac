import {Component, Input} from '@angular/core';
import {SearchBoxService} from '../services/search-box.service';

@Component({
  selector: 'sl-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent {

  @Input() noResult: boolean;

  constructor(private searchBoxService: SearchBoxService) {
  }

  searchAll(query: string) {
    this.searchBoxService.searchText$.next(query);
    this.searchBoxService.queryLength = query.length;
  }

}
