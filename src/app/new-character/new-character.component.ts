import {Component, Inject, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RestService} from '../services/rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Character} from '../models/character.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sl-new-character',
  templateUrl: './new-character.component.html',
  styleUrls: ['./new-character.component.scss']
})
export class NewCharacterComponent implements OnDestroy {
  form: FormGroup;
  id: number;
  availableSpecies: string[];
  genders: string[] = this.config.genders;
  errorMessageRequired = this.config.errorMessageRequired;
  isSubmitting = false;
  isEditing = false;
  private unsubscribe$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private restService: RestService,
    private router: Router,
    private route: ActivatedRoute,
    @Inject('CONFIG') readonly config
  ) {
    this.restService.getSpecies().pipe(takeUntil(this.unsubscribe$)).subscribe(response => this.availableSpecies = response);
    this.id = +this.route.snapshot.paramMap.get('id');
    const character = new Character();
    this.returnForm(character);
    if (this.id !== null && this.id !== 0) {
      this.restService.getCharacter(+this.id).pipe(takeUntil(this.unsubscribe$)).subscribe(
        response => {
          this.isEditing = true;
          this.returnForm(response);
        }
      );
    }

  }

  returnForm(character: Character) {
    return this.form = this.formBuilder.group({
      name: [character.name, Validators.required],
      species: [character.species, Validators.required],
      gender: [character.gender, Validators.required],
      homeworld: [character.homeworld]
    });
  }

  get name() {
    return this.form.get('name');
  }

  get species() {
    return this.form.get('species');
  }

  get gender() {
    return this.form.get('gender');
  }

  navigateToHomepage() {
    this.router.navigateByUrl('/').then();
  }

  submitNewCharacter(form) {
    if (this.form.invalid) {
      Object.keys(this.form.controls).forEach(field => {
        const control = this.form.get(field);
        control.markAsTouched();
      });
    } else {
      if (this.isEditing) {
        this.isSubmitting = true;
        this.restService.patchCharacter(this.id, form).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
          this.isSubmitting = false;
          this.navigateToHomepage();
        });
      } else {
        this.isSubmitting = true;
        this.restService.postCharacter(form).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
          this.isSubmitting = false;
          this.navigateToHomepage();
        });
      }
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
