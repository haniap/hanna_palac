import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[slFocusInput]'
})
export class FormDirective {
  constructor(private el: ElementRef) {
  }

  @HostListener('submit')
  onFormSubmit() {
    const invalidInput = this.el.nativeElement.querySelector('.is-invalid');

    if (invalidInput) {
      invalidInput.focus();
    }
  }
}
