export class Config {
  readonly restApiServer = 'http://localhost:3000';
  readonly maxItemsPerPage = 10;
  readonly genders: string[] = ['male', 'female', 'n/a'];
  readonly errorMessageRequired = 'This field is required.';
}
